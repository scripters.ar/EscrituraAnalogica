# Escritura Analogica (PWM)

En este repositorio encontraras información sobre como realizar escrituras analógicas con Arduino, controlaremos la intensidad de un led con un potenciometro.

## Materiales

* **Una placa Arduino**
* **Una resistencia de 220Ω**
* **Un Potenciometro 10k**
* **Un Led**
* **Un Protoboard**

## Uso

```
En la carpeta diagrams encontraras la conexion del circuito.
```

```
En la carpeta src encontraras el codigo fuente de ejemplo.
```

```
Una vez compilado el codigo, al mover el potenciometro el Led deberia modificar su intensidad. 
```

## Autor

* **Edermar Dominguez** - [Ederdoski](https://github.com/ederdoski)

## License

This code is open-sourced software licensed under the [MIT license.](https://opensource.org/licenses/MIT)