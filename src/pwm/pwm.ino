int potenciometro = A0;   // seleccionamos pin para el potenciometro
int led = 3;              // seleccionamos un pin para conectar el led (debe ser un pin PWM)
int val = 0;              // variable para leer estado del potenciometro

void setup() {
  pinMode(potenciometro, INPUT);    // configuramos el potenciometro como Entrada
  pinMode(led, OUTPUT);             // configuramos el led como Salida
}

void loop() {
  val = analogRead(potenciometro);
  analogWrite(led, val / 4);        //los valores de analogRead van de 0 a 1023, los valores de analogWrite de 0 a 255 por eso se divide entre 4
}

